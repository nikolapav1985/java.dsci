/**
*
* ExceptionExample class
*
* an example of exceptions and handling
*
* try to read out of bound array element
*
* if there is an error (an exception) handle it
*
* ----- compile -----
*
* javac ExceptionExample.java
*
* ----- run -----
*
* java ExceptionExample
*
*/
class ExceptionExample{
    public static void main(String args[]){
        int numbers[]={1,2,3};
        try{
            System.out.println(numbers[10]); // try to print out of bound element
        }
        catch(Exception e){ // exception happened, should be handled
            System.out.println("An error happened");
        }
    }
}
